<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <link rel="stylesheet" href="CSS/Notre entreprise.css"> 
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet"> 
    <title>Notre Entreprise </title> 
</head>
<body>
<nav> 
		<ul> 
			<li class = "Menu-déroulant"> <a href="A Propos.php">A Propos </a> 
				<ul class = "Liens-déroulants">  
				<li><a href="A Propos.php">A Propos </a></li>
				<li><a href="Notre entreprise.php">Notre Entreprise </a></li> 
				<li><a href="Nous contacter.php">Nous contacter </a></li> 
			</ul>
			</li>  
			<li class = "Menu-déroulant"> <a href="Page d'accueil Site Studio Pierrot.html">Connexion </a> 
				<ul class = "Liens-déroulants"> 
				</ul> 
			</li> 
			<li class = "Menu-déroulant"> <a href="Page d'accueil Site Studio Pierrot.html"> Inscription </a> 
				<ul class = "Liens-déroulants"> 
				</ul> 
			</li> 
			<li class = "Menu-déroulant"> <a href="index.php">Accueil </a> 
			<ul class = "Liens-déroulants"> 
			</ul> 
			<li> 
			<img class = "Logo" src="images/logo.png" width ="140" height = "52" alt=" " > 
			</li> 
			</li> 
			
		</ul> 
	</nav> 		
        <h2 class = "Notre">Notre entreprise </h2> 
    <h2>Entreprise d'animation </h2> 
    <h3> Réalisation d'animations </h3> 
    <p>Depuis sa création en 1979, Pierrot a créé plus de 100 titres d'animation, notamment des séries télévisées, des longs métrages, des émissions en direct sur vidéo et des programmes ludo-éducatifs. Notre vaste bibliothèque se compose de plus de 84 titres télévisés et de 30 longs métrages couvrant une grande variété de genres allant de la science-fiction à l'action, la romance, la comédie, le drame historique et la fantaisie. Avec les meilleurs talents créatifs du Japon et plus de 40 ans d'expérience, Pierrot continue d'être l'une des principales sociétés d'animation du Japon. </p> 
    <h3>Développement et diffusion </h3> 
    <p>En plus d'adapter des bandes dessinées et des romans populaires à des séries animées, Pierrot crée également des contenus originaux ainsi que des longs métrages, apportant toujours des expériences d'animation qui résonnent avec les tendances du divertissement en constante évolution. À travers des événements et des marchés internationaux, nous sommes pionniers sur des marchés en dehors du Japon pour amener notre animation dans le monde entier. Nous travaillons également en partenariat avec les plus grands studios du monde pour travailler sur des coproductions afin de proposer une animation de qualité de classe mondiale qui transcende les frontières. </p> 
    <h2>Entreprise de caractère </h2> 
    <h3>Licences de personnages et merchandising </h3> 
    <p>Pierrot entreprend des licences de personnages non seulement à partir des productions de Pierrot mais également à partir de livres pour enfants et d'œuvres d'art de designers. Nous travaillons avec de nombreux partenaires du monde entier pour proposer des produits sous licence allant des vêtements à la papeterie, des produits de style de vie et bien d'autres. Avec des concepteurs et des équipes d'approbation internes hautement expérimentés, nous nous assurons que nos produits sous licence sont de haute qualité en termes de qualité et de conception. </p> 
    <h3>Commercialisation et promotion </h3> 
    <p>Pierrot dispose d'une équipe marketing expérimentée qui organise des événements promotionnels et des boutiques satellites pour assurer une exposition maximale de nos produits sous licence. Nous tirons parti des événements et expositions animés pour promouvoir nos initiatives et nos produits, en tenant notre public et nos partenaires commerciaux informés de nos activités et des sorties à venir. </p> 
	<p class = "Gestion">Gestion du Studio Pierrot </p> 
</body>
</html>