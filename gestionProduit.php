<h2> Gestion des Produit de type Vetement </h2>

<?php
	if(isset($_SESSION['email']) and $_SESSION['role']=="admin"){
	$unControleur->setTable("listeProduitV");
	$leProduit = null;

	if(isset($_GET['action'])and isset($_GET['idProduit']))
	{
		$action = $_GET['action'];
		$idProduit = $_GET['idProduit'];

		switch($action)
		{
			case "sup" : 
				$where = array("idProduit"=>$idProduit);
				$unControleur->deleteProc("deleteProduitV",$where);
			break;
			case "edit" : 
				$where = array("idProduit"=>$idProduit);
				$leProduit = $unControleur->selectWhere($where);
			break;
		}
	}

	require_once ("vue/vue_insert_produit.php");

	if(isset($_POST['Modifier']))
	{
		$unControleur->setTable("listeProduitV");
		$tab = array("nom"=>$_POST['nom'],
					"taille"=>$_POST['taille'],
					"type"=>$_POST['type']);
		$where = array("idProduit"=>$_GET['idProduit']);
		$unControleur->update($tab, $where);
		header("Location: index.php?page=3");
	}

	if(isset($_POST['Valider']))
	{
		$unControleur->setTable("listeProduitV");
		$tab = array("nom"=>$_POST['nom'],
					"taille"=>$_POST['taille'],
					"quantite"=>$_POST['quantite'],
					"idtype"=>$_POST['idtype']);
		$unControleur->insertProc2("insertProduitV",$tab);
	}
}

	$unControleur->setTable("listeProduitV");
	if(isset($_POST['Rechercher']))
	{
		$tab = array("nom","taille","quantite","Type");
		$mot = $_POST['mot'];
		$lesProduits = $unControleur->selectSearch($tab, $mot);
	}
	else
	{
		$lesProduits = $unControleur->selectAll();
	}
	require_once ("vue/vue_les_produits.php");
?>
</br>
<?php
	$unControleur->setTable("listeProduitQ");
	if(isset($_POST['Rechercher']))
	{
		$tab = array("Qualite","nbCommande","QuantiteTotal");
		$mot = $_POST['mot'];
		$lesCommandes = $unControleur->selectSearch($tab, $mot);
	}
	else
	{
		$lesCommandes = $unControleur->selectAll();
	}
	require_once ("vue/vue_les_commandes.php");

?>